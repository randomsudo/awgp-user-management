import React, { Component } from 'react';
import './Home.css';
import awgplogo from '../img/logo.jpg';

class Home extends Component {
    render() {
        return (
            <div className="home-container">
                <div className="container">
                    <img src={awgplogo} alt="AWGP LOGO" />
                    <h1 className="home-title">AWGP Bangalore Login page</h1>

                </div>
            </div>
        )
    }
}

export default Home;